const model = require('../models');

module.exports = {
  getAll: () => model.car.findAll(),
  findById: (id) => model.car.findOne({ where: { id: id } }),
  create: (data) => model.car.create(data),
  update: (data) =>
    model.car.update(
      {
        name: data.name,
        price: data.price,
        size: data.size,
        photo: data.photo,
      },
      {
        where: {
          id: data.id,
        },
      }
    ),
  destroy: (id) =>
    model.car.destroy({
      where: {
        id: req.body.id,
      },
    }),
};
