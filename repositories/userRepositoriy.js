const model = require('../models');

module.exports = {
  getAll: () => model.user.findAll(),
  findById: (id) => model.user.findOne({ where: { id: id } }),
  create: (data) => model.user.create(data),
  update: (data) =>
    model.user.update(
      {
        name: data.name,
        price: data.price,
        size: data.size,
        photo: data.photo,
      },
      {
        where: {
          id: data.id,
        },
      }
    ),
  // destroy: (id) =>
  //   model.user.destroy({
  //     where: {
  //       id: req.body.id,
  //     },
  //   }),
};
