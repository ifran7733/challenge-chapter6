const express = require('express');
const router = express.Router();
const {
  list,
  create,
  update,
  destroy,
  findById,
} = require('../controllers/carController');
const validate = require('../middleware/validate');
const { createCarRules } = require('../validators/rule'); //harus di edit
const checkToken = require('../middleware/checkToken');
const checkPermision = require('../middleware/checkPermision');

router.get('/list', checkToken, list);
router.post('/find-by-id', checkToken, findById);
router.post('/create', checkToken, validate(createCarRules), create);
router.put('/update', checkToken, update);
router.delete('/destroy', checkToken, destroy);
module.exports = router;
