const express = require('express');
const router = express.Router();
const carRouter = require('./car');
const userRouter = require('./user');

router.get('/check', (req, res) => res.send('Aplication Up'));
router.use('/car', carRouter);
router.use('/user', userRouter);

module.exports = router;
