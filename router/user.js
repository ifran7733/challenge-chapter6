const express = require('express');
const router = express.Router();
const {
  register,
  login,
  getProfile,
} = require('../controllers/userController.js');
const checkToken = require('../middleware/checkToken.js');
const validate = require('../middleware/validate');
const { registerRules } = require('../validators/rule');

router.post('/register', validate(registerRules), register);
router.post('/login', login);
router.get('/profile', checkToken, getProfile);

module.exports = router;
