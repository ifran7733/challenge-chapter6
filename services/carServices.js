const repository = require('../repositories/carRepository');

module.exports = {
  getAll: () => repository.getAll(),
  findById: (id) => repository.findById(id),
};
