const repository = require('../repositories/userRepository');

module.exports = {
  getAll: () => repository.getAll(),
  findById: (id) => repository.findById(id),
};
